<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles.css">
    <title>Home</title>
  </head>
  <body>
    <div class="topNav">
      <div class="menu">
        <a href="#personal">Personal Data</a>
        <a href="#educational">Educational Background</a>
        <a href="#interest">Skills & Interest</a>
        <a href="#workex">Work Experience</a>
      </div>
    </div>
    <div class="content">
      <div class="pic">
        <img src="profpic.png" >
        <div class="layer1">
          <a href="aboutme.php">About me</a>
        </div>
      </div>

      <div class="intro">
        <h1 id = "line1">GREETINGS, I AM </h1>
        <h1 id="line2">JAMES CLAUDE LEQUIN</h1>
        <h1 id="line3"> STUDENT OF UNIVERSITY OF SOUTHEASTERN PHILIPPINES</h1>

        <br>

      <div class="social">
        <h1>Social Media</h1>
          <a href="https://www.facebook.com/1x1equals11"class="icon"> <img src="fb.png"> </a>
          <a href="mailto:lequinj7@gmail.com"class="icon"> <img src="gmail.png"> </a>
          <a href="newpage.html"class="icon"> <img src="tweet.png"> </a>

        </div>
      </div>
    </div>

    <br><br>

    <div class="header" id="personal">
      <h1>PERSONAL DATA</h1>
    </div>

    <div id="parag">
      <p>
        Full name: James Claude Bongaitan Lequin <br>
        Birth date: October 4, 1998 <br>
        Place of birth: Davao City <br>
        Age: 19 <br>
        Sex: Male <br>
        Citizenship: Filipino <br>
        Email: lequinj7@gmail.com <br>
        Course: Bachelor of Science in Information Technology
      </p>
    </div>

    <br><br>

    <div class="header" id="educational">
      <h1>EDUCATIONAL BACKGROUND</h1>
    </div>

    <div id="parag">
      <p>
        Tertiary: Univerity of Southeastern Philippines (2016-present)<br>
        Secondary: Davao City National High School (2011-2015)<br>
        Primary: Kapitan Tomas Monteverde Sr. Central Elementary School (2009-2011)<br>
        Universit of Mindanao (2005-2009)
      </p>
    </div>

    <div class="header" id="interest">
      <h1>SKILLS & INTEREST</h1>
    </div>

    <div id="parag">
      <p>
        Programming <br>
        Photo Editing <br>
        Bowling <br>
        Traveling <br>
        Eating <br>
        Sleeping <br>
        Playing online games <br>
      </p>
    </div>

    <div class="header" id="workex">
      <h1>WORK EXPERIENCE</h1>
    </div>

    <div id="parag">
      <p>Data Encoder</p>
    </div>

  </body>
</html>
