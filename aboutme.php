<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles.css">
    <title>About me</title>
  </head>
  <body>
    <div class="topNav">
      <div class="menu">
        <a href="#personal">Personal Data</a>
        <a href="#educational">Educational Background</a>
        <a href="#interest">Skills & Interest</a>
        <a href="#workex">Work Experience</a>
      </div>
    </div>

    <div class="content">
      <div class="pic">
        <img src="C:\Users\Claude\Desktop\GitFolder\new-project\profpic.png" >
        <div class="layer1">
          <a href="aboutme.html">About me</a>
        </div>
      </div>

      <div class="intro">
        <h1 id = "line1">GREETINGS, I AM </h1>
        <h1 id="line2">JAMES CLAUDE LEQUIN</h1>
        <h1 id="line3"> STUDENT OF UNIVERSITY OF SOUTHEASTERN PHILIPPINES</h1>

        <br>

      <div class="social">
        <h1>Social Media</h1>
          <a href="https://www.facebook.com/1x1equals11"class="icon"> <img src="C:\Users\Claude\Desktop\GitFolder\new-project\fb.png"> </a>
          <a href="mailto:lequinj7@gmail.com"class="icon"> <img src="C:\Users\Claude\Desktop\GitFolder\new-project\gmail.png"> </a>
          <a href="newpage.html"class="icon"> <img src="C:\Users\Claude\Desktop\GitFolder\new-project\tweet.png"> </a>

        </div>
      </div>
    </div>
  </body>
</html>
